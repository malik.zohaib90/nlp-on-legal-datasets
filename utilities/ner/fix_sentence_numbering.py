#This script takes tagged data by nltk/spacy as input, as performed by notebooks 
# ../jupyter_notebooks/Label_NER_with_NLTK.ipynb and ../jupyter_notebooks/Label_NER_with_Spacy.ipynb


# Output: It numbers the sentences again, hence fixing any incorrect numbering  that may have occured 
# while fixing the dataset manually, or as a result of removing short sentences. It writes output
# with correct numbering in a separate file


import re
input_filename = "../../data/ner/ner_tagged_data_with_nltk_spacy_reviewed_manually.csv"
with open(input_filename, 'r') as f:
	content = f.readlines()

print(len(content))

sentences = 0
curr_sentence=0

for x in range(0,len(content)):
	# Sentence: 1,This,O

	sentence_number = content[x].split(",")[0]
	matches = re.findall('Sentence: [0-9]+,*', sentence_number)
	if matches:
		sentences = sentences + 1
		curr_sentence = curr_sentence + 1
		for match in matches:
			# print(match)
			# print(content[x])
			content[x]=content[x].replace(
				"Sentence: "+str(match.split(" ")[-1])+",",
				"Sentence: "+str(curr_sentence)+","
				)

print("total sentences = "+str(sentences))



out_filename = "../../data/ner/ner_tagged_data_with_nltk_spacy_reviewed_manually_1.csv"
with open(out_filename, 'w') as out_file:
	out_file.writelines(content)