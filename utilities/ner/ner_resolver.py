import os

# defining a lambda function for length comparison
longer = lambda x,y: x if x>y else y

#defining a method to resolve the conflicts between tagged sentences
def resolve_ner_tags(spacy_tags, nltk_tags):
	if len(spacy_tags) == len(nltk_tags):
		resolved_sentence = []
		for s,n in zip(spacy_tags, nltk_tags):
			if s == n:
				resolved_sentence.append(s) #both libraries agree on this token, can take from token from spacy or nltk both. this line chooses token from spacy.
			elif s.strip().endswith(",O"):
				resolved_sentence.append(n) #take tag from nltk
			elif n.strip().endswith(",O"):
				resolved_sentence.append(s) #take tag from spacy
			else:
				resolved_sentence.append(s.strip()+"--"+n.strip().split(",")[-1]+"\n") #include both
		return resolved_sentence
	else:
		return []
#read input file of labelled ner by spacy
spacy_filename = "../../data/ner/ner_tagged_data_with_spacy.csv"
with open(spacy_filename, 'r') as f:
	spacy_content = f.readlines()
#read input file of labelled ner by nltk
nltk_filename = "../../data/ner/ner_tagged_data_with_nltk.csv"
with open(nltk_filename, 'r') as f:
	nltk_content = f.readlines()

#start resolving the conflicts:
print("resolving ner conflicts . . .")
spacy_cursor = 0
nltk_cursor = 0
spacy_sentence = []
nltk_sentence = []
resolved_content = []
try:
	for cursor in range(0,longer(len(nltk_content),len(spacy_content))-1):
		
		if nltk_cursor>=len(nltk_content) and spacy_cursor>=len(spacy_content): #marks the end of both cursors reading tagged data from both libraries. we should stop at this point.
			print("Done!")
			break

		#<spacy world>
		if spacy_content[spacy_cursor]!="BLANK,BLANK,BLANK\n":
			#we have read the complete sentence
			spacy_sentence.append(spacy_content[spacy_cursor])
			spacy_cursor+=1
		#</spacy world>

		#<nltk world>
		if nltk_content[nltk_cursor]!="BLANK,BLANK,BLANK\n":
			#we have read the complete sentence
			nltk_sentence.append(nltk_content[nltk_cursor])
			nltk_cursor+=1
		#</nltk world>

		#<common world>
		if spacy_content[spacy_cursor]=="BLANK,BLANK,BLANK\n" and nltk_content[nltk_cursor]=="BLANK,BLANK,BLANK\n":
			spacy_sentence.append(spacy_content[spacy_cursor]) #adding the sentence separator
			nltk_sentence.append(nltk_content[nltk_cursor]) #adding the sentence separator
			
			#moving on to the next sentence
			nltk_cursor+=1
			spacy_cursor+=1
			
			if resolve_ner_tags(spacy_sentence, nltk_sentence):
				resolved_content.extend(resolve_ner_tags(spacy_sentence, nltk_sentence))
			nltk_sentence = []
			spacy_sentence = []
		#</common world>
except:
	#diagnose the cursors problems if any occur at the end of files.
	print("Cursors not traversed propertly. analyze the cursor positions below")
	print("spacy cursor: "+str(spacy_cursor))
	print("nltk cursor: "+str(nltk_cursor))
	print("general cursor: "+str(cursor))


#save the final resolved content:
out_filename = "../../data/ner/ner_tagged_data_with_nltk_spacy.csv"
with open(out_filename, 'w') as out_file:
	out_file.writelines(resolved_content)

print("find the result in file: "+str(os.path.abspath(out_filename)))
