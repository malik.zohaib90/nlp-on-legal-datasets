'''

This file is used for creating classfication data for judgement types, from the datasets downloaded from CaseLaw Access Project, in json format.
The output from this file can be next used to run classification task.

## Command to run
python create_judgement_types_dataset.py -i <inputfile>

Required Parameters:
-i is the comma separated paths to input json files containing data from CaseLaw

Optional Parameters:
-d the type of decission for which we need to create the dataset, e.g. majority/dissent. default is majority
-l limit the judgements, the script will create this many datapoints for the decision type provided via -d parameter. default is 10000
-q is used for verbose, if you want the script to run silently, add -q to the command. (important info will still be printed)

Note:
We create dataset files for each type of decision, containing number of datapoints as provided by -l option, these dataset files can be used to read and shuffle together in order to run the classification task

'''





from custom_option_parser import PassThroughOptionParser
import re
import json
import nltk.data
import sys
import os
from gensim.summarization.summarizer import summarize
import pandas as pd

#color coding
class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'



#parser for input arguments to the script file.
parser = PassThroughOptionParser(usage="python create_judgement_types_dataset.py -i <inputfiles>. For more Help, run as: python create_judgement_types_dataset.py -h")
parser.add_option("-i", "--input", dest="input",
                  help="the comma separated input dataset files, (.jsonl files, as downloaded from case.law)", metavar="STRING")
parser.add_option("-d", "--decision_type", dest="decision_type",
                  help="the type of decission, e.g. majority/dissent", metavar="STRING", default="majority")
parser.add_option("-l", "--decisions_limit", dest="decisions_limit",
                  help="number of decisions of this type to include in dataset", metavar="INT", default=10000)
parser.add_option("-q", "--quiet", action="store_false", dest="verbose", default=True,
                  help="don't print long error messages to stdout")

#we only use options, so args are neglected if any are provided.
(options, args) = parser.parse_args()
parser.validate_opts(options, {"input" : "-i"})

input_data_files = options.input
decision_type = options.decision_type
decisions_limit = int(options.decisions_limit)
verbose = options.verbose

if decision_type not in ["majority", "dissent"]:
	print(f"{bcolors.FAIL}Decision Type can be either majority or dissent. No decision type available as: {decision_type}{bcolors.ENDC}")
	exit(0)

print("\n\nStart creating classification dataset for judgements using following configurations:\n")


input_file_list = input_data_files.split(",")
print(f"{bcolors.BOLD}Input Dataset/s to be used = {input_file_list}{bcolors.ENDC}")
print(f"{bcolors.BOLD}\nDecision Type = {str(decision_type)}{bcolors.ENDC}")
print(f"{bcolors.BOLD}\nDecisions limit = {str(decisions_limit)}{bcolors.ENDC}")

input(f"{bcolors.OKBLUE}\n\nPress Enter to continue...{bcolors.ENDC}")


separator="\t"
HEADER = "id"+separator+"sentence"+separator+"label"+"\n"
path_to_output_files = os.path.join("tagged_data","classification")
if not os.path.exists(path_to_output_files):
	os.makedirs(path_to_output_files)

OUTPUT_FILE = os.path.join(path_to_output_files, "judgement_type_"+decision_type+".tsv")
out_file = open(OUTPUT_FILE,"w")
out_file.write(HEADER)

#calculates the ratio to which the summarized version of a particular decision reduced. Longer the decision, higher will be the reduction ratio.
reduction_factor=80 #we simply divide this by the length of the decision, and keep the percentage of decision in the summary, to the result we get from this division.


def clean(text):
	text = re.sub(r'\[[0-9]*\]', ' ', text)
	text = re.sub(r'\s+', ' ', text)
	text = re.sub('[^a-zA-Z.,"]', ' ', text)
	text = re.sub(r'\s+', ' ', text)
	return text


#just to keep stats on the dataset we prepare, e.g. number of total words in input decision, number of words after summarization, compression ratio for a particular decision.
_words_total = []
_length = []
_words_summarized = []
compression_ratio=[]

#unique row identifier in the output dataset file
_id = 0

for input_data_file in input_data_files.split(","):

	if _id>=decisions_limit: #break the outer loop as well if cases limit has been reached. more details on similar check in the inner loop.
			break
	with open(input_data_file, 'r') as f:
	    content = f.readlines()
	print("total cases in input file "+input_data_file+": "+str(len(content)))

	for i, record in enumerate(content):
		jsonobject = json.loads(record)
		opinions = jsonobject['casebody']['data']['opinions']
		if _id>=decisions_limit: #Stop after the required number of decisions of ${decision_type} have been found.
			break
		if _id%(decisions_limit/50)==0:
			print("Decisions written: "+str(_id))
		for j, opinion in enumerate(opinions):
			if opinion['type'] is not None and opinion['type'] in [decision_type]: #dissent, majority
				try:

					sentence=summarize(clean(opinion['text']), ratio=reduction_factor/len(opinion['text'].split(" ")))
					sentence=sentence.replace("\n",". ")

					if len(sentence.split(" "))<5:
						continue
					_words_total.append(len(opinion['text'].split(" ")))
					_length.append(len(opinion['text']))
					compression_ratio.append(reduction_factor/len(opinion['text'].split(" ")))
					# if opinion['type'] in sentence:
					
					out_file.write(str(_id)+separator+sentence+separator+opinion['type']+"\n")
					_id += 1

					_words_summarized.append(len(sentence.split(" ")))
				except:
					if verbose:
						print("\nCould not summarize: "+str(opinion['text']))
						print("\n")

df = pd.DataFrame({'_words_total':_words_total,'_length':_length, '_words_summarized':_words_summarized, 'compression_ratio': compression_ratio})

df.to_csv(str(decision_type)+"_stats.tsv", sep=separator)


print(f"{bcolors.OKGREEN}\nDataset file created at: {str(out_file.name)}{bcolors.ENDC}")
print(f"{bcolors.OKGREEN}\nDetailed Stats for this dataset in file: {str(decision_type)}_stats.tsv{bcolors.ENDC}")
