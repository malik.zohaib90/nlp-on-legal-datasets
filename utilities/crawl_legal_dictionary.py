"""

This script crawls the dictionary from https://law.com to create a legal vocabulary

Output: A TSV file containing list of legal vocabulary with definitions.

"""

from bs4 import BeautifulSoup
import requests
import os

#url to retrieve all legal vocabulary by starting letter
dictionary_url = "https://dictionary.law.com/Default.aspx?letter=A"
alphabets = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 
'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'Y', 'Z']

f = open("../data/legal_vocabulary/multi-tokens_legal_dictionary.tsv", "w")
f.write("Word\tDefinition\n")

for alphabet in alphabets:
	print("Requesting url: "+str(dictionary_url.replace("A",alphabet)))
	r = requests.get(dictionary_url.replace("A",alphabet))

	html_content = r.text

	soup = BeautifulSoup(html_content, "lxml")


	words = soup.findAll("span", attrs={"class": "word"})
	definitions = soup.findAll("span", attrs={"class": "definition"})

	for word,definition in zip(words,definitions):
		# if len(word.text.strip().split())==1:
		f.write(str(word.text.strip())+"\t"+str(definition.text.strip())+"\n")

print("Extracted legal dictionary: "+str(os.path.realpath(f.name)))
f.close()