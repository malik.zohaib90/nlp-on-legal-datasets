
'''
This file is used for creating pre-training data from the datasets downloaded from CaseLaw Access Project, in json format.
The output from this file can be next used to run pre-training process on BERT [look at the documentation provided by authors of BERT, to understand how to do it]



## Command to run
python generate_pretraining_data.py -i <inputfile> -o <outputfile>


Required Parameters:
-i is the path to directory containing input json files, or just the complete path to file containing data from CaseLaw
-o is the path to output directory/file where you want to save the pre-training data

Notes:
if you provide directory with -i, script will read all files with .jsonl extension in the directoy. Single file if the path including file name is provided
if you provide directory with -o, script will write all output files in this directory, with a pattern given by -p and file size determined by -r optional parameters. Single file if the path including file name is provided.


Optional Parameters:
-q is used for verbose, if you want the script to run silently, add -q to the command. (important info will still be printed)
-l limit the cases. default is 10000000
-s skip the cases. default is 0 (you can combine this with limit (-l) to get pagination feature)
-p in case if you are saving output in multiple files, provide a naming pattern and script will use it as prefix for output files
-r in case if you are saving output in multiple files, -r provides the limit of lines in a single output file before script rotates to another file. default is 500000

'''

from custom_option_parser import PassThroughOptionParser
import json
import nltk.data
import preprocessing
from datetime import datetime
import os
import glob

#parser for input arguments to the script file.
parser = PassThroughOptionParser(usage="python sentence_segmentation.py -i <inputfile/dir> -o <outputfile/dir>")
parser.add_option("-i", "--input", dest="input",
                  help="the input dataset file or directory", metavar="STRING")
parser.add_option("-o", "--output", dest="output",
                  help="the input file or directory", metavar="STRING")
parser.add_option("-p", "--output_file_pattern", dest="output_file_pattern", default="pre-training-legaldata",
                  help="the name pattern for output text file/s", metavar="STRING")
parser.add_option("-l", "--cases_limit", dest="cases_limit",
                  help="number of cases to process", metavar="INT", default=10000000)
parser.add_option("-s", "--skip", dest="cases_skip",
                  help="number of cases to process", metavar="INT", default=0)
parser.add_option("-r", "--lines_limit", dest="lines_limit",
                  help="number of lines in a single file", metavar="INT", default=500000)
parser.add_option("-q", "--quiet", action="store_false", dest="verbose", default=True,
                  help="don't print status messages to stdout")

#we only use options, so args are neglected if any are provided.
(options, args) = parser.parse_args()
parser.validate_opts(options, {"output" : "-o", "input" : "-i"})

#save the options given from command line now that we have parsed and validated them
_input = options.input
_output = options.output
verbose = options.verbose
cases_limit = int(options.cases_limit)
cases_skip = int(options.cases_skip)
output_file_pattern = options.output_file_pattern
#define the lines limit after which output file should be rotated
lines_limit = int(options.lines_limit)
input_files = []
output_files = []

print("Start generating pre-training data using following configurations: ")
if os.path.isfile(_input):
	print("Input data file: ")
	input_files.append(_input)
else :
	print("list of input files from directory \""+str(_input)+"\" : ")
	input_files.extend(glob.glob(str(_input)+"/*.jsonl"))
print(str([i_file.split("/")[-1] for i_file in input_files]))

if os.path.isfile(_output):
	print("output file = "+str(_output))
else :
	print("output directory = "+str(_output))

print("cases_limit = "+str(cases_limit))
print("cases_skip = "+str(cases_skip))
print("output_file_pattern = "+str(output_file_pattern))
print("lines_limit = "+str(lines_limit))



input("Press Enter to continue...")#at this point, you can halt the script by ctrl+c, if any changes are necessary in the input parameters.


#some stats
#sentences_included -> total sentences that would be written in the final output file:
sentences_built = 0
sentences_included = 0
sentences_filtered = 0

#for color coding on the console to distinguish important outputs messages.
class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

#returns number of integers, alphabets, spaces, punctuations in a sentence. e.g. happened in 1920. returns 4, 10, 2, 1
def count_chars(s):

	numbers = sum(c.isdigit() for c in s)
	alphabets   = sum(c.isalpha() for c in s)
	spaces  = sum(c.isspace() for c in s)
	punctuations  = len(s) - numbers - alphabets - spaces

	return numbers, alphabets, spaces, punctuations

# takes a sentence, and returns percentages of integers, alphabets, spaces, punctuations in it.
def calculate_percentages(sentence):
	numbers, alphabets, spaces, punctuations = count_chars(sentence)
	total = numbers + alphabets + spaces + punctuations
	numbers = round((numbers/total)*100, 2)
	alphabets = round((alphabets/total)*100, 2)
	spaces = round((spaces/total)*100, 2)
	punctuations = round((punctuations/total)*100, 2)
	return numbers, alphabets, spaces, punctuations


# Returns true if this sentence should be discarded, false otherwise.
'''
Filter Rules: Discard
1: the short sentences.
2: the low quality text. includes mostly the referrences and not enough percentage of legal text in the sentence.
'''
def filter_sentence(sentence):
	if len(sentence.split(" "))<3: # discard sentences if they are too short.
		return True
	numbers, alphabets, spaces, punctuations = calculate_percentages(sentence)
	return punctuations>=10 or numbers>=20 or (punctuations+numbers)>=25 or (numbers + punctuations) >= 15


#for sentence segmentation
tokenizer = nltk.data.load('tokenizers/punkt/english.pickle')

curr_out_file = None
if os.path.isfile(_output) or ("." in _output and "/" not in _output):
	curr_out_file = open(_output,"w")
else:
	if not os.path.exists(_output):
		os.makedirs(_output)
	curr_out_file = open(os.path.join(_output, output_file_pattern+"_0.txt"),"w")

def close_file():
	global sentences_included
	if curr_out_file is not None and not curr_out_file.closed:
		curr_out_file.close()
		output_files.append((curr_out_file.name,sentences_included))

#rotates the output file if is is needed. verifies the fact that user wants to get multiple output files, and current output file has exceeded the limit defined
def rotate_outputfile(sentences_included):
	global curr_out_file
	if os.path.isdir(_output):
		if lines_limit<=sentences_included:
			if verbose:
				print("limit of rotation exceeded: current file name: "+str(curr_out_file.name))
			next_count = int(curr_out_file.name.split("_")[-1].split(".")[0])+1
			output_files.append((curr_out_file.name,sentences_included))
			curr_out_file.close()

			curr_out_file = open(os.path.join(_output,output_file_pattern+"_"+str(next_count)+".txt"),"w")
			if verbose:
				print("next file name: "+str(curr_out_file.name))
			sentences_included = 0
	return sentences_included, curr_out_file


#takes list of lines already in a format containing necessary newline and delimiter characters
def write_in_file(lines):
	global sentences_included
	sentences_included, out_file = rotate_outputfile(sentences_included)
	sentences_included= sentences_included + len(lines)
	for line in lines:
		out_file.write(line)


#print the output files statistics. names of files, sentences added in them, total sentences included for this run
def print_stats(sentences_built):
	print("Filename\t\t\t :  Lines Written")
	sentences_included = 0
	for file in output_files:
		print(str(file[0])+"\t\t\t :  "+str(file[1]))
		sentences_included = sentences_included + file[1]

	print("sentences_built = "+str(sentences_built))
	print(f"{bcolors.OKGREEN}total sentences_included in output data = {sentences_included}{bcolors.ENDC}")
	print("sentences_filtered = "+str(sentences_built - sentences_included))

def finalize(sentences_built,current_time):
	close_file()
	print_stats(sentences_built)
	print(f"{bcolors.OKBLUE}Data generation took time {datetime.now() - current_time}{bcolors.ENDC}")
	exit(0)

def printProgressBar (iteration, total, prefix = '', suffix = '', decimals = 1, length = 100, fill = '█'):
    """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
    """
    percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
    filledLength = int(length * iteration // total)
    bar = fill * filledLength + '-' * (length - filledLength)
    print('\r%s |%s| %s%% %s' % (prefix, bar, percent, suffix), end = '\r')
    # Print New Line on Complete
    if iteration == total:
        print()

#note the start time
current_time = datetime.now()

for file_number, input_filename in enumerate(input_files):
	new_doc = False
	#read the content, and display the stats from the dataset
	with open(input_filename, 'r') as f:
	    content = f.readlines()
	    f.close()
	print("Processing file "+str(file_number+1)+" out of "+str(len(input_files))+", file name =  "+str(input_filename.split("/")[-1])+", total cases in this file = "+str(len(content)))
	if not verbose:
		printProgressBar(0, len(content), prefix = ' Progress:', suffix = 'Complete', length = 50)
	
	for j, line in enumerate(content):

		if j<cases_skip:
			continue
		if not verbose:
			printProgressBar(j+1, len(content), prefix = ' Progress:', suffix = 'Complete', length = 50)

		jsonobject = json.loads(line)
		opinions = jsonobject['casebody']['data']['opinions']
		lines_list = []
		for i, opinion in enumerate(opinions):
			text = opinion['text']
			#text in judges' opinions the dataset contains \n characters, which eventually provides the wrong information while writing a single sentence(having \n characters) in the same line.
			text = preprocessing.clean_text(text)

			sentences = tokenizer.tokenize(text)
			for sentence in sentences:
				if len(sentence)>10:
					sentences_built = sentences_built + 1
					filter_this_sentence = filter_sentence(sentence)
					if not filter_this_sentence: #we are keeping this sentence in final output
						new_doc = False
						lines_list.append(sentence+"\n")
					else: # we discard this sentence from final output, hence starting a new document in pre-training data. flush the list of sentences to file, and clear the list
						if not new_doc and len(lines_list) > 10: # only add if the document exceeds at least from 10 sentences
							lines_list.append("\n")
							write_in_file(lines_list)
							if verbose:
								print("\n\nNEXT DOCUMENT (due to filter):\n\n")
							new_doc = True
						lines_list = []

		if not new_doc and len(lines_list) > 10: # only add if the document exceeds at least from 10 sentences
			lines_list.append("\n")
			write_in_file(lines_list)
			if verbose:
				print("\n\nNEXT DOCUMENT:\n\n")
			new_doc = True
			lines_list = []
		#We create the sample data only for a limited number of cases, as doing this for the complete data will generate a huge dataset for which much better performance hardware [TPUs] would be required.
		if (j-cases_skip)>=cases_limit: #don't count the cases that we skipped
			finalize(sentences_built, current_time)

#if the script was not finished because the cases limit had exceeded
finalize(sentences_built, current_time)
