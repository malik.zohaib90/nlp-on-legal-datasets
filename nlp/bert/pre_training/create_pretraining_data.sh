#bash script to create pretraining tfrecord data from plain text files containing legal data adhering to BERT'S format requirements.
#downloads bert model and bert repo.
#expects plain text files of legal data in ./input_legal_data/pre-training_data/ directory. Required format for file naming: pre-training-legaldata_[1-9][0-9]*.txt
#produces tf_record files in ./tf_record_pretraining_files/ directory, and later moves them to /bachup directory for using the bachup mechanism. Output file naming format: tf_examples_[1-9][0-9]*.tfrecord


#To be run on Cloud GPU (preferably on the GPU at chair with backup mechanism)

STARTDATE=`date +"%Y-%m-%d %T"`
echo "Current Date and Time is: "${STARTDATE}



printf "\n\n Performing environment checks"

#checkout bert_repo if it does not exist already
[ ! -d bert_repo ] && printf "\n\n<======= downloading bert_repo from git =======>\n\n\n" && git clone https://github.com/google-research/bert bert_repo
#download bert model if it does not exist already
[ ! -f input_model/uncased_L-8_H-512_A-8.zip ] && printf "\n\n<======= downloading bert model  =======>\n\n\n" && wget https://storage.googleapis.com/bert_models/2020_02_20/uncased_L-8_H-512_A-8.zip -P input_model/
#create the input_model/uncased_L-8_H-512_A-8 directory if it does not exist.
[ ! -d input_model/uncased_L-8_H-512_A-8 ] && mkdir input_model/uncased_L-8_H-512_A-8
#extract the download bert model if it is not extracted already
[ ! -f input_model/uncased_L-8_H-512_A-8/vocab.txt ] && printf "\n\n<======= extracting bert model into directory 'input_model/uncased_L-8_H-512_A-8' =======>\n\n\n" && unzip input_model/uncased_L-8_H-512_A-8.zip -d input_model/uncased_L-8_H-512_A-8/

#check if the legal data is available
[ -z "$(ls -A input_legal_data/)" ] && printf "FATAL: input_legal_data directory is empty. Can't proceed. Please provide legal data in this directory" && exit
#create the tf_record_pretraining_files directory if it does not exist.
[ ! -d tf_record_pretraining_files ] && mkdir tf_record_pretraining_files

printf "\n\n\n Exporting required enviroment variables \n"
export BERT_MODEL_PATH='./input_model/uncased_L-8_H-512_A-8'

printf "\n\nAll set!!\nStarting the Process for creating pre_training tf_record data\n "


## declare the file naming pattern for text(input) and tf_record(output) file names
input_name="./input_legal_data/pre-training_data/pre-training-legaldata_"
output_name="./tf_record_pretraining_files/tf_examples_"

batch_size=10
#as we have total 289 plain text files available
total_files=289

input_file_names=""
output_file_names=""


for i in $(seq 1 $total_files);
do
	if ! ((i % $batch_size)); then
    	#create pretraining tfrecord data for this batch
    	input_file_names=${input_file_names::-1}
		output_file_names=${output_file_names::-1}
    	printf "creating pretraining file with input_file_names and output_file_names: "
		PRE_TRAINING_DATASET=$input_file_names
		TFRECORD_CHECKPOINT=$output_file_names
		printf "\n"$PRE_TRAINING_DATASET
    	printf "\n"$TFRECORD_CHECKPOINT
		printf "\n\n"

		python bert_repo/create_pretraining_data.py \
		--input_file=$PRE_TRAINING_DATASET \
		--output_file=$TFRECORD_CHECKPOINT \
		--vocab_file=$BERT_MODEL_PATH/vocab.txt \
		--do_lower_case=False \
		--max_seq_length=128 \
		--max_predictions_per_seq=20 \
		--masked_lm_prob=0.15 \
		--random_seed=12345 \
		--dupe_factor=5

    	#clear the files done in this iteration
    	input_file_names=""
		output_file_names=""

		printf "\n\n\n"
		sleep 300
		cp ./tf_record_pretraining_files/* /backup/zohaib/bert_pretraining/tf_record_data/
		sleep 300
		du -sh ./tf_record_pretraining_files/*
		printf "\n\n\n"
		rm ./tf_record_pretraining_files/*
	fi

	#concatinate the file names to create a batch of input output files for create pretraining process
	input_file_names=$input_file_names$input_name$i.txt,
	output_file_names=$output_file_names$output_name$i.tfrecord,
done

printf "\nThe remaining files: \n"
input_file_names=${input_file_names::-1}
output_file_names=${output_file_names::-1}

PRE_TRAINING_DATASET=$input_file_names
TFRECORD_CHECKPOINT=$output_file_names
printf "\n"$PRE_TRAINING_DATASET
printf "\n"$TFRECORD_CHECKPOINT


python bert_repo/create_pretraining_data.py \
  --input_file=$PRE_TRAINING_DATASET \
  --output_file=$TFRECORD_CHECKPOINT \
  --vocab_file=$BERT_MODEL_PATH/vocab.txt \
  --do_lower_case=False \
  --max_seq_length=128 \
  --max_predictions_per_seq=20 \
  --masked_lm_prob=0.15 \
  --random_seed=12345 \
  --dupe_factor=5

printf "\n\n\n"
sleep 100
cp ./tf_record_pretraining_files/* /backup/zohaib/bert_pretraining/tf_record_data/
sleep 300
du -sh ./tf_record_pretraining_files/*
printf "\n\n\n"
rm ./tf_record_pretraining_files/*

printf "\n\n\nDone!!\n"

printf "start time"
echo ${STARTDATE}
printf "end time"
date