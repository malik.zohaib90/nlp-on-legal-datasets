# Classification Results #



[Population of Legal Decisions](https://gitlab.com/malik.zohaib90/nlp-on-legal-datasets/-/blob/master/nlp/statistics/classification/decisions_population.png?raw=true)


### XLNET_BASE ###
XLNET Base Cased Model, as released by authors.

Average Training and Validation time for 10 Epochs is approx __3 hrs__. Average is taken from 10 executions.


##### Accuracy on Test Dataset #####

    0.9421404682274247

##### Classification Report #####
                     precision    recall    f1-score   support

        majority     0.9193       0.9656    0.9419      1452
         dissent     0.9659       0.9200    0.9424      1538

        accuracy                            0.9421      2990
       macro avg     0.9426       0.9428    0.9421      2990
    weighted avg     0.9433       0.9421    0.9421      2990



    
[Confusion Matrix](https://gitlab.com/malik.zohaib90/nlp-on-legal-datasets/-/blob/master/nlp/statistics/classification/xlnet/confusion_matrix.png?raw=true) for Classification Results from XLNET_BASE

<!--- [Graph - Training vs Validation](https://gitlab.com/malik.zohaib90/nlp-on-legal-datasets/-/blob/master/nlp/statistics/classification/bert-base/training_vs_validation_accuracy.png) for 10 training and validation epochs with XLNET_BASE --->


---


### BERT_BASE ###
Bert Base Cased Model, as released by authors.

Average Training and Validation time for 10 Epochs is approx __2 hrs__. Average is taken from 10 executions.


##### Accuracy on Test Dataset #####

    0.9414715719063544

##### Classification Report #####
                     precision    recall  f1-score   support

         dissent     0.9364       0.9482    0.9423      1506
        majority     0.9468       0.9346    0.9407      1484

        accuracy                            0.9415      2990
       macro avg     0.9416       0.9414    0.9415      2990
    weighted avg     0.9415       0.9415    0.9415      2990


    
[Confusion Matrix](https://gitlab.com/malik.zohaib90/nlp-on-legal-datasets/-/blob/master/nlp/statistics/classification/bert-base/confusion_matrix.png?raw=true) for Classification Results from BERT_BASE

<!--- [Graph - Training vs Validation](https://gitlab.com/malik.zohaib90/nlp-on-legal-datasets/-/blob/master/nlp/statistics/classification/bert-base/training_vs_validation_accuracy.png) for 10 training and validation epochs with BERT_BASE --->


---


### BERT_MEDIUM ###

Bert Medium Model, as released by authors.

Average Training and Validation time for 10 Epochs is approx __40 mins__. Average is taken from 10 executions.

##### Accuracy on Test Dataset #####

    0.9391304347826086

##### Classification Report #####
                     precision    recall  f1-score   support

         dissent     0.9390       0.9402    0.9396      1506
        majority     0.9393       0.9380    0.9386      1484

        accuracy                            0.9391      2990
       macro avg     0.9391       0.9391    0.9391      2990
    weighted avg     0.9391       0.9391    0.9391      2990

    
[Confusion Matrix](https://gitlab.com/malik.zohaib90/nlp-on-legal-datasets/-/blob/master/nlp/statistics/classification/bert-medium/confusion_matrix.png?raw=true) for Classification Results from BERT_MEDIUM

<!--- [Graph - Training vs Validation](https://gitlab.com/malik.zohaib90/nlp-on-legal-datasets/-/blob/master/nlp/statistics/classification/bert-medium/training_vs_validation_accuracy.png) for 10 training and validation epochs with BERT_MEDIUM --->


---


### BERT_MEDIUM_LEGAL ###
Bert Medium Model, as released by authors, was further pre-trained on legal dataset using 1 million training steps, to produce BERT_MEDIUM_LEGAL Model.

Average Training and Validation time for 10 Epochs is approx __41 mins__. Average is taken from 10 executions.

##### Accuracy on Test Dataset #####

    0.9508361204013377

##### Classification Report #####
                     precision    recall  f1-score   support

         dissent     0.9613       0.9402    0.9507      1506
        majority     0.9407       0.9616    0.9510      1484

        accuracy                            0.9508      2990
       macro avg     0.9510       0.9509    0.9508      2990
    weighted avg     0.9511       0.9508    0.9508      2990

##### Accuracy on Test Dataset with 0.3 dropout #####

    0.9491638795986621


    
[Confusion Matrix](https://gitlab.com/malik.zohaib90/nlp-on-legal-datasets/-/blob/master/nlp/statistics/classification/bert-medium-legal/confusion_matrix.png?raw=true) for Classification Results from BERT_MEDIUM_LEGAL

<!--- [Graph - Training vs Validation](https://gitlab.com/malik.zohaib90/nlp-on-legal-datasets/-/blob/master/nlp/statistics/classification/bert-medium-legal/training_vs_validation_accuracy.png) for 10 training and validation epochs with BERT_MEDIUM_LEGAL --->


---

 

### BERT_MEDIUM_VOCAB ###

Bert Medium Model, as released by authors, was fed with specialized Legal Vocabulary to produce BERT_MEDIUM_VOCAB Model.

Average Training and Validation time for 10 Epochs is approx __52 mins__. Average is taken from 10 executions.

##### Accuracy on Test Dataset #####

    0.9434782608695652

##### Classification Report #####
                     precision    recall  f1-score   support

         dissent     0.9472       0.9402    0.9437      1506
        majority     0.9398       0.9468    0.9433      1484

        accuracy                            0.9435      2990
       macro avg     0.9435       0.9435    0.9435      2990
    weighted avg     0.9435       0.9435    0.9435      2990

    
[Confusion Matrix](https://gitlab.com/malik.zohaib90/nlp-on-legal-datasets/-/blob/master/nlp/statistics/classification/bert-medium-vocab/confusion_matrix.png?raw=true) for Classification Results from BERT_MEDIUM_VOCAB

<!--- [Graph - Training vs Validation](https://gitlab.com/malik.zohaib90/nlp-on-legal-datasets/-/blob/master/nlp/statistics/classification/bert-medium-vocab/training_vs_validation_accuracy.png) for 10 training and validation epochs with BERT_MEDIUM_VOCAB --->


---


### BERT_MEDIUM_LEGAL_VOCAB ###

Bert Medium Model, as released by authors, was further pre-trained on legal dataset using 1 million training steps, and then fed with specialized Legal Vocabulary to produce BERT_MEDIUM_LEGAL_VOCAB Model.

Average Training and Validation time for 10 Epochs is approx __51 mins__. Average is taken from 10 executions.
##### Accuracy on Test Dataset #####

    0.9528428093645485

##### Classification Report #####
                     precision    recall  f1-score   support

         dissent     0.9608       0.9449    0.9528      1506
        majority     0.9450       0.9609    0.9529      1484

        accuracy                            0.9528      2990
       macro avg     0.9529       0.9529    0.9528      2990
    weighted avg     0.9530       0.9528    0.9528      2990


    
[Confusion Matrix](https://gitlab.com/malik.zohaib90/nlp-on-legal-datasets/-/blob/master/nlp/statistics/classification/bert-medium-legal-vocab/confusion_matrix.png?raw=true) for Classification Results from BERT_MEDIUM_LEGAL_VOCAB

<!--- [Graph - Training vs Validation](https://gitlab.com/malik.zohaib90/nlp-on-legal-datasets/-/blob/master/nlp/statistics/classification/bert-medium-legal-vocab/training_vs_validation_accuracy.png) for 10 training and validation epochs with BERT_MEDIUM_LEGAL_VOCAB --->


---


### LEGAL_BERT_BASE_NLPAUEB ###

This model was prepared by [Chalkidis et al](https://arxiv.org/abs/2010.02559#:~:text=We%20also%20propose%20a%20broader,law%2C%20and%20legal%20technology%20applications.). Using Bert Base Model and pre-training it on various legal texts for 1 million training steps


Average Training and Validation time is the same as [BERT_BASE](https://gitlab.com/malik.zohaib90/nlp-on-legal-datasets/-/blob/master/nlp/results/Classification_Results.md#bert_base)
##### Accuracy on Test Dataset #####

    0.9518394648829431

##### Classification Report #####
                     precision    recall  f1-score   support

         dissent     0.9710       0.9323    0.9512      1506
        majority     0.9339       0.9717    0.9524      1484

        accuracy                            0.9518      2990
       macro avg     0.9524       0.9520    0.9518      2990
    weighted avg     0.9526       0.9518    0.9518      2990

---
