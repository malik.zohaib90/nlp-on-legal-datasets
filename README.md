# Contributions #

Repository provides the implementation for the work required to answer following research questions:


__Question 1:__ Which of the Transformer based Language Models such as [BERT](https://github.com/google-research/bert) and [XLNet](https://github.com/zihangdai/xlnet) perform better in Legal Domain?

__Question 2:__ How does Legal Vocabulary affect the performance of BERT on Legal Tech?

__Question 3:__ Is additional pre-training with Legal data on top of an existing pre-trained general language model of BERT worthwhile for optimizing NLP performance in Legal Tech?


Models prepared during the research are available in [models](/models) subdirectory.

